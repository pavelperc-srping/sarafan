package com.pavelperc.tutors.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.pavelperc.tutors.domain.JsonViews;
import com.pavelperc.tutors.domain.Message;
import com.pavelperc.tutors.repo.MessageRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("message")
public class MessageController {
    
    private final MessageRepo messageRepo;
    
    @Autowired
    public MessageController(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }
    
    
    // return all messages
    @GetMapping
    @JsonView(JsonViews.IdName.class)
    public List<Message> list() {
        return messageRepo.findAll();
    }
    
    @GetMapping("{id}")
    public Message getOne(@PathVariable("id") Message message) {
        // Сам додумался найти message по id, но нужна подсказка - переменная id из getMapping
        return message;
    }
    
    @PostMapping
    public Message create(@RequestBody Message message) {
        // Id нам не пришло, но если и пришло, то на него не смотрим
        message.setCreationDate(LocalDateTime.now());
        return messageRepo.save(message);
    }
    
    @PutMapping("{id}")
    public Message update(@PathVariable("id") Message messageFromDB, @RequestBody Message message) {
        BeanUtils.copyProperties(message, messageFromDB, "id");
        
        return messageRepo.save(messageFromDB);
    }
    
    
    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Message message) {
        messageRepo.delete(message);
    }
    
    
}
