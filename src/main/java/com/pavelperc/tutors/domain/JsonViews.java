package com.pavelperc.tutors.domain;

public final class JsonViews {
    
    // marker to convert in json only id and name
    public interface IdName {}
    
    public interface FullMessage extends IdName {}
}
