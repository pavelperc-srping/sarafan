package com.pavelperc.tutors.repo;

import com.pavelperc.tutors.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepo extends JpaRepository<Message, Long> {
    
}
